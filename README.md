# ml-simian

##### Associar variável SIMIAN_API com URL raiz da aplicação 
* export SIMIAN_API=http://localhost:8080

##### Curl para enviar cadeia de DNA para validação na api 
* curl -X POST -H "Content-Type: application/json" --data '{ "dna": ["ACCATT","CATCTA","CCTCAT","TTCCCA","ATCCAA","AAACCC"] }' $SIMIAN_API/simian

##### Curl para obter as estatisticas das valições
* curl -X GET $SIMIAN_API/stats