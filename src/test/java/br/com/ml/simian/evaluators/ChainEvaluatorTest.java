package br.com.ml.simian.evaluators;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ChainEvaluatorTest {
	final ChainEvaluator chainEvaluator = new ChainEvaluator();
	
	@Test
	public void find_horizontal_chain_at_begining() {
		assertTrue(chainEvaluator.evaluate("CCCCTA".split("")));
	}
	
	@Test
	public void find_horizontal_chain_at_end() {
		assertTrue(chainEvaluator.evaluate("TACCCC".split("")));
	}
	
	@Test
	public void find_horizontal_chain_at_middle() {
		assertTrue(chainEvaluator.evaluate("TCCCCA".split("")));
	}
}
