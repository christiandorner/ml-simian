package br.com.ml.simian.evaluators;

import static br.com.ml.simian.matrix.DiagonalDirection.DOWN_TOP;
import static br.com.ml.simian.matrix.DiagonalDirection.TOP_DOWN;

import org.junit.Assert;
import org.junit.Test;

import br.com.ml.simian.matrix.Matrix;

public class DiagonalChainTest {

	private DiagonalChainEvaluator diagonal = new DiagonalChainEvaluator();

	@Test
	public void find_one_diagonal_top_down_right_left() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "T", "G", "A", "G", "A"},
			new String[] {"C", "T", "A", "T", "G", "C"},
			new String[] {"T", "A", "T", "T", "G", "T"},
			new String[] {"A", "G", "A", "G", "G", "G"},
			new String[] {"C", "C", "C", "C", "T", "A"},
			new String[] {"T", "C", "A", "C", "T", "G"}
		}), TOP_DOWN);
		
		Assert.assertEquals(1, create);
	}
		
	@Test
	public void find_two_diagonal_top_down_right_left() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "T", "G", "A", "G", "A"},
			new String[] {"C", "T", "A", "T", "G", "C"},
			new String[] {"T", "A", "T", "G", "G", "T"},
			new String[] {"A", "G", "G", "G", "G", "G"},
			new String[] {"C", "G", "C", "C", "T", "A"},
			new String[] {"T", "C", "A", "C", "T", "G"}
		}), TOP_DOWN);
		
		Assert.assertEquals(2, create);
	}
	
	@Test
	public void find_three_diagonal_top_down_right_left() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "T", "G", "A", "G", "A"},
			new String[] {"C", "T", "A", "T", "G", "C"},
			new String[] {"T", "A", "T", "G", "G", "T"},
			new String[] {"A", "G", "G", "G", "T", "G"},
			new String[] {"C", "G", "C", "T", "T", "A"},
			new String[] {"T", "C", "T", "C", "T", "G"}
		}), TOP_DOWN);
		
		Assert.assertEquals(3, create);
	}
	
	@Test
	public void find_none_diagonal_down_top() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "T", "G", "A", "G", "A"},
			new String[] {"C", "T", "A", "T", "G", "C"},
			new String[] {"T", "C", "T", "T", "G", "T"},
			new String[] {"A", "G", "A", "G", "G", "G"},
			new String[] {"C", "C", "C", "C", "T", "A"},
			new String[] {"T", "C", "A", "C", "T", "G"}
		}), DOWN_TOP);
		
		Assert.assertEquals(0, create);
	}
	
	@Test
	public void find_one_diagonal_down_top() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "T", "G", "A", "G", "A"},
			new String[] {"C", "T", "A", "T", "G", "C"},
			new String[] {"T", "C", "T", "T", "G", "T"},
			new String[] {"A", "G", "C", "G", "G", "G"},
			new String[] {"C", "C", "C", "C", "T", "A"},
			new String[] {"T", "C", "A", "C", "T", "G"}
		}), DOWN_TOP);
		
		Assert.assertEquals(1, create);
	}
	
	@Test
	public void find_two_diagonal_down_top() {
		final int create = diagonal.evaluate(new Matrix(new String[][] {
			new String[] {"C", "G", "G", "A", "G", "A"},
			new String[] {"C", "T", "G", "T", "G", "C"},
			new String[] {"T", "C", "T", "G", "G", "T"},
			new String[] {"A", "G", "C", "G", "G", "G"},
			new String[] {"C", "C", "C", "C", "T", "A"},
			new String[] {"T", "C", "A", "C", "T", "G"}
		}), DOWN_TOP);
		
		Assert.assertEquals(2, create);
	}
}