package br.com.ml.simian.evaluators;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SimianDNAEvaluatorTest {

	private SimianDNAEvaluator evaluator = new SimianDNAEvaluator();
	
	@Test
	public void ensure_simian_horizontal_nitrogen_base() {
		final boolean isSimian = evaluator.isSimian(new String[] { 
				"CTGTAA", 
				"TTTTGC", 
				"TATTGT", 
				"AGAGGG", 
				"CCCCTA", 
				"TCACTG" });
		assertTrue(isSimian);
	}
	
	@Test
	public void ensure_simian_vertical_nitrogen_base() {
		final boolean isSimian = evaluator.isSimian(new String[] { 
				"CAGTAA", 
				"TATTGC", 
				"TATTGG", 
				"AAAGGG", 
				"ACTCTG", 
				"TCACTG" });
		assertTrue(isSimian);
	}
	
	@Test
	public void ensure_simian_diagonal_top_down_nitrogen_base() {
		final boolean isSimian = evaluator.isSimian(new String[] { 
				"CTGATA", 
				"TTATGC", 
				"TATTGA", 
				"AGAGAG", 
				"CCTATA", 
				"TCACTG" });
		assertTrue(isSimian);
	}
	
	@Test
	public void ensure_simian_diagonal_down_top_nitrogen_base() {
		final boolean isSimian = evaluator.isSimian(new String[] { 
				"CTATAA", 
				"TTAAGC", 
				"TATTAT", 
				"ATAGGA", 
				"CTTCTA", 
				"TCATTG" });
		assertTrue(isSimian);
	}
	
	@Test
	public void ensure_human_when_no_chain() {
		final boolean isSimian = evaluator.isSimian(new String[] { 
				"CTGTAA", 
				"TTAAGC", 
				"TATTAT", 
				"AAAGGA", 
				"CTTCTA", 
				"TCATTG" });
		assertFalse(isSimian);
	}
}
