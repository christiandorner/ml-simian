package br.com.ml.simian.rest;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import br.com.ml.simian.domain.Dna;
import br.com.ml.simian.domain.Stats;
import br.com.ml.simian.evaluators.SimianDNAEvaluator;
import br.com.ml.simian.repository.SimianRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = Service.class)
public class ServiceTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	public SimianDNAEvaluator simianDNAEvaluator;
	
	@MockBean
    private SimianRepository simianRepository;

	@Test
	public void should_403_when_lower_case_chain() throws Exception {
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"aTGCGA\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"] }"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	public void should_403_when_chain_is_less_then_length() throws Exception {
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"ATGCG\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"] }"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	public void should_200_when_previous_found_and_is_simian() throws Exception {
		final List<String> chain = Arrays.asList("ATGCCG", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG");
		final Dna dna = new Dna(UUID.randomUUID(), chain, true, chain.hashCode());
		when(simianRepository.findByHash(dna.hash())).thenReturn(Optional.of(dna));
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"ATGCCG\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"] }"))
			.andExpect(status().isOk());
	}
	
	@Test
	public void should_403_when_previous_found_and_is_not_simian() throws Exception {
		final List<String> chain = Arrays.asList("ATGCCG", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG");
		final Dna dna = new Dna(UUID.randomUUID(), chain, false, chain.hashCode());
		when(simianRepository.findByHash(dna.hash())).thenReturn(Optional.of(dna));
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"ATGCCG\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"] }"))
			.andExpect(status().isForbidden());
	}
	
	@Test
	public void should_save_new_simian_update_stats() throws Exception {
		final List<String> chain = Arrays.asList("ATGCCG", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG");
		final Dna dna = new Dna(UUID.randomUUID(), chain, false, chain.hashCode());
		when(simianRepository.findByHash(dna.hash())).thenReturn(Optional.empty());
		when(simianDNAEvaluator.isSimian(dna.chain().toArray(String[]::new))).thenReturn(true);
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"ATGCCG\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCCTA\", \"TCACTG\"] }"))
			.andExpect(status().isOk());
		verify(simianRepository).save(Mockito.any(Dna.class));
		verify(simianRepository).updateStats(Mockito.any(Dna.class));
	}
	
	@Test
	public void should_save_new_human_update_stats() throws Exception {
		final List<String> chain = Arrays.asList("ATGCCG", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG");
		final Dna dna = new Dna(UUID.randomUUID(), chain, false, chain.hashCode());
		when(simianRepository.findByHash(dna.hash())).thenReturn(Optional.empty());
		this.mockMvc.perform(
				 post("/simian")
				.contentType(APPLICATION_JSON)
				.content("{ \"dna\": [\"ATGCCG\", \"CAGTGC\", \"TTATGT\", \"AGAAGG\", \"CCCGTA\", \"TCACTG\"] }"))
			.andExpect(status().isForbidden());
		verify(simianRepository).save(Mockito.any(Dna.class));
		verify(simianRepository).updateStats(Mockito.any(Dna.class));
	}
	
	@Test
	public void should_return_stats() throws Exception {
		when(simianRepository.stats()).thenReturn(new Stats(UUID.randomUUID(), Float.valueOf(1), Float.valueOf(2), 0.5));
		this.mockMvc.perform(
				 get("/stats"))
			.andExpect(status().isOk())
			.andExpect(content().json("{\"count_mutant_dna\": 1, \"count_human_dna\": 2, \"ratio\": 0.5}"));
		verify(simianRepository).stats();
	}
}
 