package br.com.ml.simian.matrix;

import org.junit.Assert;
import org.junit.Test;

public class MatrixTest {

	@Test
	public void find_second_line_horizontal_values() {
		final String[] verticalized = new Matrix(new String[] { "CTGAGA", "CTGAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG" }).horizontal(1);
		Assert.assertArrayEquals(new String[] { "C", "T", "G", "A", "G", "C"}, verticalized);
	}
	
	@Test
	public void find_first_column_values() {
		final String[] verticalized = new Matrix(new String[] { "CTGAGA", "CTGAGC", "TATTGT", "AGAGGG", "CCCCTA", "TCACTG" }).vertical(0);
		Assert.assertArrayEquals(new String[] { "C", "C", "T", "A", "C", "T"}, verticalized);
	}

	@Test
	public void find_values_at_position_3_5() {
		final String value = new Matrix(new String[] { "CTGAGA", "CTGAGC", "TATTGT", "AGAGTG", "CCCCTA", "TCACTG" }).get(3, 4);
		Assert.assertEquals("T", value);
	}
}