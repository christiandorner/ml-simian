package br.com.ml.simian.evaluators;

import br.com.ml.simian.matrix.DiagonalDirection;
import br.com.ml.simian.matrix.Matrix;

public class DiagonalChainEvaluator {

	public int evaluate(Matrix matrix, DiagonalDirection direction) {
		int diagonalChain = 0;
		for (int i = direction.startingPoint(matrix); direction.endReached(matrix, i); i = direction.step(i)) {
			for (int j = 3; j < matrix.length(); j++) {
				final String baseValue = matrix.get(i, j);

				int row = direction.nextRow(i);
				int col = j - 1;
				int chain = 1;
				while (true) {
					if(row < 0 || row == matrix.length() || col < 0 || col == matrix.length()) break;
					final String matching = matrix.get(row, col);
					if(baseValue.equals(matching)) {
						chain++;
						row = direction.nextRow(row);
						col--;
					} else break;
				}
				if(chain >= 4) diagonalChain++; 
			}
			
		}
		return diagonalChain;
	}
	
}