package br.com.ml.simian.evaluators;

public class ChainEvaluator {

	public boolean evaluate(String[] line) {
		for (int i = 0; i < line.length; i++) {
			String c = line[i];

			int horizontalAlignment = 1;
			int chain = 1;
			while ((horizontalAlignment + i) < line.length && c.equals(line[i + horizontalAlignment])) {
				chain++;
				horizontalAlignment++;
			}
			if (chain >= 4)
				return true;
		}

		return false;
	}
}