package br.com.ml.simian.evaluators;

import static br.com.ml.simian.matrix.DiagonalDirection.DOWN_TOP;
import static br.com.ml.simian.matrix.DiagonalDirection.TOP_DOWN;

import org.springframework.stereotype.Component;

import br.com.ml.simian.matrix.Matrix;

@Component
public class SimianDNAEvaluator {

	private ChainEvaluator chainEvaluator = new ChainEvaluator();
	private DiagonalChainEvaluator diagonalChainEvaluator = new DiagonalChainEvaluator();

	public boolean isSimian(String[] dna) {
		final Matrix matrix = new Matrix(dna);
		
		int chain = 0;
		for (int i = 0; i < matrix.length(); i++) {
			if (chainEvaluator.evaluate(matrix.horizontal(i)))
				chain++;
		}

		if (isSimian(chain)) return true;

		for (int i = 0; i < matrix.length(); i++) {
			if (chainEvaluator.evaluate(matrix.vertical(i)))
				chain++;
		}
		
		if (isSimian(chain)) return true;

		chain += diagonalChainEvaluator.evaluate(matrix, TOP_DOWN);
		if (isSimian(chain)) return true;

		chain += diagonalChainEvaluator.evaluate(matrix, DOWN_TOP);
		
		return isSimian(chain);
	}

	private boolean isSimian(int chain) {
		return chain > 1;
	}

}
