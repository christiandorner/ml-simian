package br.com.ml.simian;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;

@Configuration
@EnableMongoRepositories(basePackages = { "br.com.ml.simian.repository" })
public class DatabaseActivator extends AbstractMongoConfiguration {

	@Value("${spring.database.mongo.servers}")
	private String mongoServers;

	@Value("${spring.database.mongo.name}")
	private String databaseName;

	public @Bean MongoClient mongoClient() {
		final MongoClientOptions options = new MongoClientOptions.Builder()
			.minConnectionsPerHost(5) //
			.connectionsPerHost(200) //
			.build();
		return new MongoClient(mongoServers, options);
	}

	public @Bean MongoTemplate mongoTemplate() {
		MongoTemplate mongoTemplate = new MongoTemplate(mongoClient(), databaseName);
		MappingMongoConverter mongoMapping = (MappingMongoConverter) mongoTemplate.getConverter();
		mongoMapping.setCustomConversions(customConversions());
		mongoMapping.afterPropertiesSet();
		return mongoTemplate;
	}

	@Override
	protected String getDatabaseName() {
		return databaseName;
	}
	
}
