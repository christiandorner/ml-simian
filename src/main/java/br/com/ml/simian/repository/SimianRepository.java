package br.com.ml.simian.repository;

import static java.util.Optional.ofNullable;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;
import static org.springframework.data.mongodb.core.query.Update.update;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;

import br.com.ml.simian.domain.Dna;
import br.com.ml.simian.domain.Stats;

@Repository
public class SimianRepository {

	private MongoOperations mongoOperations;

	@Autowired
	public SimianRepository(MongoOperations mongoOperations) {
		this.mongoOperations = mongoOperations;
	}

	public Optional<Dna> findByHash(int hash){
		return ofNullable(mongoOperations.findOne(query(where("hash").is(hash)), Dna.class));
	}
	
	public void save(Dna dna) {
		mongoOperations.save(dna);
	}

	public void updateStats(Dna dna) {
		mongoOperations.upsert(query(where("id").is(Stats.STATS_UUID)),
				update("id", Stats.STATS_UUID)
					.inc(dna.isSimian() ? "count_mutant_dna" : "count_human_dna", 1), Stats.class);
	}

	public Stats stats() {
		return Optional.ofNullable(mongoOperations.findOne(query(where("id").is(Stats.STATS_UUID)), Stats.class)).orElse(Stats.STATS);
	}
}
