package br.com.ml.simian;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Activator  {

	public static void main(String[] args) {
        SpringApplication.run(Activator.class, args);
    }

}