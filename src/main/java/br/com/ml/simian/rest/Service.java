package br.com.ml.simian.rest;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.ml.simian.domain.Dna;
import br.com.ml.simian.domain.Stats;
import br.com.ml.simian.evaluators.SimianDNAEvaluator;
import br.com.ml.simian.repository.SimianRepository;

@RestController
public class Service {

	private final SimianDNAEvaluator evaluator;
	private final SimianRepository simianRepository;

	@Autowired
	public Service(SimianDNAEvaluator evaluator, SimianRepository simianRepository) {
		this.evaluator = evaluator;
		this.simianRepository = simianRepository;
	}
	
	@PostMapping(value = "/simian")
	public ResponseEntity<String> isSimian(@RequestBody DNA dnaData) {
		if(!dnaData.isValid()) return new ResponseEntity<String>(FORBIDDEN);
		
		final Optional<Dna> found = simianRepository.findByHash(dnaData.hash());
		if(!found.isEmpty()) return new ResponseEntity<String>(found.get().isSimian() ? OK : FORBIDDEN); 

		final boolean isSimian = evaluator.isSimian(dnaData.getDna().toArray(String[]::new));
		final Dna dna = new Dna(UUID.randomUUID(), dnaData.getDna(), isSimian, dnaData.hash());
		simianRepository.save(dna);
		simianRepository.updateStats(dna);
		return new ResponseEntity<String>(isSimian ? OK : FORBIDDEN);
	}
	
	@GetMapping(value = "/stats", produces = APPLICATION_JSON_VALUE)
	public ResponseEntity<StatsData> stats(){
		final Stats stats = simianRepository.stats();
		return new ResponseEntity<StatsData>(new StatsData(stats.mutant().intValue(), stats.human().intValue(), stats.ratio()), OK);
	}
}
