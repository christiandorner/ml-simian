package br.com.ml.simian.rest;

import java.util.List;
import java.util.regex.Pattern;

import lombok.Getter;
import lombok.Setter;

public class DNA {
	private static final Pattern PATTERN = Pattern.compile("[^ATCG]");
	
	@Getter @Setter
	private List<String> dna;
	

	public int hash() {
		return dna.hashCode();
	}
	
	public boolean isValid() {
		return !dna.stream().anyMatch(PATTERN.asPredicate().or(s -> s.length() != dna.size()));
	}
}