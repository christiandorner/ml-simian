package br.com.ml.simian.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class StatsData {
	
	@JsonProperty("count_mutant_dna")
	private final int mutant;
	
	@JsonProperty("count_human_dna")
	private final int human;
	
	@JsonProperty("ratio")
	private final double ratio;
}
