package br.com.ml.simian.domain;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@RequiredArgsConstructor
@Accessors(fluent = true)
@Document("dnas")
public class Dna implements Serializable {
	private static final long serialVersionUID = -4017630462470414007L;
	
	@Id
	private final UUID id;
	
	private final List<String> chain;
	
	private final boolean isSimian;
	
	@Indexed
	private final int hash;


}
