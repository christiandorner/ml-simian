package br.com.ml.simian.domain;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(fluent = true)
@Document("stats")
public class Stats implements Serializable {
	private static final long serialVersionUID = -4017630462470414007L;
	
	public static final UUID STATS_UUID = UUID.fromString("db458c78-c128-11e9-947c-0bc129176592");
	public static final Stats STATS = new Stats();
	
	@Id
	private UUID id = STATS_UUID;
	
	@Field("count_mutant_dna")
	private Float mutant = Float.valueOf(0);
	
	@Field("count_human_dna")
	private Float human = Float.valueOf(0);
	
	@Transient
	private double ratio;

	public double ratio() {
		return (mutant == 0 || human == 0) ? 0 : mutant / human;
	}
	
}
