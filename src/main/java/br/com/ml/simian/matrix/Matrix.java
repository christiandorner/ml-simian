package br.com.ml.simian.matrix;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Matrix {
	
	private final String[][] matrix;
	
	public Matrix(String[] matrix) {
		this.matrix = new SingletonMatrix().matrix(matrix);
	}
	
	public String[] horizontal(int row) {
		final String[] horizontal = new String[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			horizontal[i] = matrix[row][i];
		}
		return horizontal;
	}

	public String[] vertical(int col) {
		final String[] vertical = new String[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			vertical[i] = matrix[i][col];
		}
		return vertical;
	}
	
	public int length() {
		return matrix.length;
	}

	public String get(int i, int j) {
		return matrix[i][j];
	}

	
}