package br.com.ml.simian.matrix;

import java.util.Map;

import com.google.common.collect.ImmutableMap;

public class SingletonMatrix {
	private Map<String, String> m = ImmutableMap.of("A", "A", "T", "T", "C", "C", "G", "G");

	public String[][] matrix(String[] values) {
		final String[][] matrix = new String[values.length][values[0].length()];

		for (int i = 0; i < values.length; i++) {
			final String[] split = values[i].split("");
			for (int j = 0; j < split.length; j++) {
				matrix[i][j] = m.get(split[j]);
			}
		}

		return matrix;
	}
}