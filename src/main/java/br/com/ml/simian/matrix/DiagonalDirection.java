package br.com.ml.simian.matrix;

public enum DiagonalDirection {
	TOP_DOWN {
		public int startingPoint(Matrix matrix) { return 0; }

		public int nextRow(int i) { return i + 1; }

		public int step(int i) { return ++i; }

		public boolean endReached(Matrix matrix, int i) { return i < matrix.length() - 1; }
		
	}, DOWN_TOP {
		public int startingPoint(Matrix matrix) { return matrix.length() - 1; }

		public int nextRow(int i) { return i - 1; }

		public int step(int i) { return --i; }

		public boolean endReached(Matrix matrix, int i) { return i > 0; }
	};
	
	public abstract int startingPoint(Matrix matrix);
	
	public abstract int nextRow(int i);

	public abstract int step(int i);

	public abstract boolean endReached(Matrix matrix, int i);
}